/*aqui eu escolhi fazer dessa forma, pois caso a api viesse a ter
 novos endpoints, só seria necessário adicionar uma linha por endpoint e
 não uma função inteira, outra coisa é que a logica de resposta como o status http
 fica apenas aqui
 */
const { httpStatus } = require('./src/constants/httpStatus');
const { billetValidator } = require('./src/api/billetValidator');
const { createResponse } = require('./src/utils/response');

const handler = async (event, action) => {
  const { data, errors } = await action(event);
  if (errors) {
    return createResponse(httpStatus.BAD_REQUEST, { errors });
  }

  return createResponse(httpStatus.OK, data);
};

module.exports = {
  billetValidator: async (event) => handler(event, billetValidator),
};
