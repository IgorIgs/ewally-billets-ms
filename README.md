## How To Run

### Install dependencies
For this you will need to have installed NodeJS V10. If you don't have it installed, go to <https://nodejs.org/en/> and install it. 

Once you have it installed, run `npm install` to install the necessary packages

```
npm install
```
Then, install the serverless framework CLI globally
```
npm install serverless -g
``` 
You will need to have your AWS Credentials configured in order to use the serverless offline plugin and to deploy your functions 
For this, go to <https://serverless.com/framework/docs/providers/aws/guide/credentials/> and see how to set the necessary environment variables
```
export AWS_ACCESS_KEY_ID='YOUR_ACCESS_KEY_ID'
export AWS_SECRET_ACCESS_KEY='YOUR_SECRET_ACCESS_KEY'
```
run the command
```

serverless offline
```

Endpoints
```

GET http://localhost:3000/development/billetValidator?digitableLine={YOUR_DIGITABLE_LINE}
```