module.exports.formatValue = (value) => {
  while (value.length) {
    if (value[0] != 0) break;
    value = value.replace('0', '');
  }

  let length = value.length;

  if (length <= 0) {
    return 'Valor não encontrado';
  }

  if (length <= 5) {
    return `${value.slice(0, length - 2)},${value.slice(length - 2, length)}`;
  }

  if (length <= 9) {
    return `${value.slice(0, length - 5)}.${value.slice(length - 5, length - 2)},${value.slice(length - 2, length)}`;
  }
};

module.exports.formatDate = (date) => {
  const day = date.getDate() > 9 ? date.getDate() : `0${date.getDate()}`;
  const month = date.getMonth() >= 9 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`;
  const year = date.getFullYear();

  return `${day}/${month}/${year}`;
};
