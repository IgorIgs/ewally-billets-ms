module.exports.module10 = (numbers) => {
  let dv = numbers[numbers.length - 1];

  let lengthWithoutDV = numbers.length - 2;
  let multiplier = 2;
  let sumResults = 0;

  for (i = lengthWithoutDV; i >= 0; i--) {
    value = numbers[i] * multiplier;

    sumResults += value < 10 ? value : 1 + (value - 10);
    multiplier = multiplier === 1 ? 2 : 1;
  }

  let result = 10 - (sumResults % 10);

  if (result === 10) result = 0;

  return dv == result;
};
