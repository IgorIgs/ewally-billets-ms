module.exports.module11 = (numbers, isBanckBonds = false) => {
  dv = isBanckBonds ? numbers.slice(4, 5) : numbers[numbers.length - 1];
  numbers = isBanckBonds ? numbers.slice(0, 4) + numbers.slice(5, 44) : numbers.slice(0, 44);

  multiplier = 2;
  sumResults = 0;

  for (let i = numbers.length - 1; i >= 0; i--) {
    sumResults += numbers[i] * multiplier;
    multiplier = multiplier < 9 ? ++multiplier : 2;
  }

  const restOfDivision = sumResults % 11;
  let result = 11 - restOfDivision;

  if (result > 9 || result == 0) return 1;
  return result == dv;
};
