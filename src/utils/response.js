module.exports.createResponse = (statusCode, body) => {
  return {
    statusCode,
    body: JSON.stringify(body),
  };
};
