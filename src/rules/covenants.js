const { module10 } = require('../utils/module10');
const { module11 } = require('../utils/module11');
const { formatValue, formatDate } = require('../utils/formatting');

module.exports.processCovenants = (digitableLine) => {
  field3Value = digitableLine.slice(2, 3);

  let isModule10 = false;

  if (field3Value == 6 || field3Value == 7) {
    isModule10 = true;
  } else if (field3Value != 8 && field3Value != 9) {
    return { errors: { mensagem: 'Linha digitavel invalida' } };
  }

  const block1 = digitableLine.slice(0, 12);
  const block2 = digitableLine.slice(12, 24);
  const block3 = digitableLine.slice(24, 36);
  const block4 = digitableLine.slice(36, 48);

  const invalidBlocks = [block1, block2, block3, block4].filter((block) =>
    isModule10 ? !module10(block) : !module11(block)
  );

  if (invalidBlocks.length) {
    return { errors: { mensagem: 'Linha digitavel invalida' } };
  }

  const barcode =
    digitableLine.slice(0, 11) +
    digitableLine.slice(12, 23) +
    digitableLine.slice(24, 35) +
    digitableLine.slice(36, 47);

  const value = formatValue(barcode.slice(5, 15));

  const year = barcode.slice(19, 22);
  const month = barcode.slice(23, 25);
  const day = barcode.slice(25, 27);

  dateValid = year >= 1997 && month <= 12 && day <= 31;

  const dueDate = dateValid ? formatDate(new Date(year, month - 1, day)) : 'Data Não encontrada';

  return {
    data: {
      digitableLineIsValid: true,
      barcode,
      dueDate,
      value,
    },
  };
};
