const { module10 } = require('../utils/module10');
const { module11 } = require('../utils/module11');

const { formatValue, formatDate } = require('../utils/formatting');

module.exports.processBankBonds = (digitableLine) => {
  const block1 = digitableLine.slice(0, 10);
  const block2 = digitableLine.slice(10, 21);
  const block3 = digitableLine.slice(21, 32);

  const invalidFields = [block1, block2, block3].filter((field) => !module10(field));

  if (invalidFields.length) {
    return { errors: { mensagem: 'Linha digitavel invalida' } };
  }

  const dueDateFactor = digitableLine.slice(33, 37);
  const billetValue = digitableLine.slice(37, 48);

  const barcode =
    digitableLine.slice(0, 4) +
    digitableLine.slice(32, 33) +
    dueDateFactor +
    billetValue +
    digitableLine.slice(4, 9) +
    digitableLine.slice(10, 20) +
    digitableLine.slice(21, 31);

  if (!module11(barcode, (isBanckBonds = true))) {
    return { errors: { mensagem: 'Linha digitavel invalida' } };
  }

  const dueDate = processDueDateFactor(dueDateFactor);
  const value = formatValue(billetValue);

  return {
    data: {
      digitableLineIsValid: true,
      barcode,
      dueDate,
      value,
    },
  };
};

/*
  escolhi deixar essa função nesse mesmo arquivo, pois só ele utiliza essa função e 
  por ele ter uma logica bem especifica, provavelmente ele não será utilizados por outros
  metodos futuramente */

function processDueDateFactor(factor) {
  console.log(factor);
  const date = new Date(1997, 9, 7);
  date.setDate(date.getDate() + Number(factor));

  return formatDate(date);
}
