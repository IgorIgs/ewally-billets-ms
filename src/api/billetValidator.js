/* 
  esse arquivo tem apenas a lógica de validar o campo de entrada e
  saber qual método ele tem que chamar para processar aquele tipo de boleto.
  Eu fiz dessa forma com intuito de deixar aplicação mais limpa e de melhor entendimento e 
  caso viesse a ter novos modelos de boletos com novas lógicas,
  seria apenas adicionar mais duas linhas
 */

const { processBankBonds } = require('../rules/bankBonds');
const { processCovenants } = require('../rules/covenants');

const billetValidator = (event) => {
  const { queryStringParameters } = event;

  if (!queryStringParameters || !queryStringParameters.digitableLine) {
    return { errors: { mensagem: 'O parametro digitableLine é obrigatorio' } };
  }

  const { digitableLine } = queryStringParameters;

  numberWithoutPunctuation = digitableLine.replace(/[^0-9]/g, '');
  switch (numberWithoutPunctuation.length) {
    case 48:
      return processCovenants(numberWithoutPunctuation);

    case 47:
      return processBankBonds(numberWithoutPunctuation);

    default:
      return { errors: { mensagem: 'Linha digitavel invalida' } };
  }
};

module.exports = {
  billetValidator,
};
